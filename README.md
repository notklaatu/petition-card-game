# Petition

Prove your faith. Bid for divine favour. Sacrifice citizens to save your culture in a game of calculated risk and deadly fanaticism.

Petition is an [Open Game
License](http://www.opengamingfoundation.org/ogl.html) and [Creative
Commons](https://creativecommons.org/licenses/by-sa/4.0) card game. In
it, players fight for the favour of one of four gods so that they may
defeat their opponent's civilisation by slaughtering their military, subverting their economy, appropriating their arts and culture, replacing their religion. It's imperialist and fundamentalist fun for the whole family!

2-6 players have been play-tested. There is also a solo mod.


## Rules

Multi-player and single-player rules are included in this repository in the ``doc`` directory.


## Assets

This repository uses [git-portal](https://gitlab.com/notklaatu/git-portal) to manage its graphical files, so you must download the art files separately.


## Open source

Petition was build using a fully open source stack, including Linux, GIMP, Inkscape, Krita, Scribus, and Emacs.


## Reuse

Everything in **Petition** is covered under a [copyleft](https://www.gnu.org/licenses/copyleft.en.html) license, meaning you can use, modify, redistribute, and even sell anything you find here. However, you must offer your work under the same licenses.